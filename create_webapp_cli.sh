


## perpare index.html example page

cat <<EOF > index.html
<!DOCTYPE html>
<html>
<head>
    <title>My Demo App</title>
</head>
<body>
    <h1>Hello, Azure App Service!</h1>
</body>
</html>
EOF

## login az to get started
az login


## create an rs group in Amsterdam
az group create --name mywebapprs --location northeurope


## create an webapp plan name
az appservice plan create --resource-group mywebapprs --name mywebappplanname

## Creates a webapp with mywebappname in rg mywebapprs
az webapp create --name mywebappname --resource-group mywebapprs --plan mywebappplanname


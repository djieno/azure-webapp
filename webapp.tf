provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "example" {
  name     = "myResourceGroup"
  location = "East US"
}

resource "azurerm_app_service_plan" "example" {
  name                = "myAppServicePlan"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  kind                = "Linux"
  reserved            = false

  sku {
    tier = "Basic"
    size = "B1"
  }
}

resource "azurerm_app_service" "example" {
  name                = "myWebApp"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  app_service_plan_id = azurerm_app_service_plan.example.id

  site_config {
    always_on = true
  }

  app_settings = {
    "WEBSITE_NODE_DEFAULT_VERSION" = "14"
  }

  connection_string {
    name  = "AzureWebJobsStorage"
    type  = "Custom"
    value = "DefaultEndpointsProtocol=https;AccountName=myaccount;AccountKey=mykey;EndpointSuffix=core.windows.net"
  }

  logs {
    error {
      detailed_error_messages_enabled = false
      failed_request_tracing_enabled   = false
    }
  }
}

resource "azurerm_app_service_source_control" "example" {
  app_service_id = azurerm_app_service.example.id
  repo_url       = "https://github.com/YourUsername/YourRepo.git"
  branch         = "main"
  is_manual_integration = false
}

resource "azurerm_app_service_custom_hostname_binding" "example" {
  hostname = "www.example.com"
  web_app_id = azurerm_app_service.example.id
  resource_group_name = azurerm_resource_group.example.name
}

resource "azurerm_app_service_custom_hostname_binding" "example" {
  hostname = "example.com"
  web_app_id = azurerm_app_service.example.id
  resource_group_name = azurerm_resource_group.example.name
}

resource "azurerm_app_service_custom_hostname_binding" "example" {
  hostname = "subdomain.example.com"
  web_app_id = azurerm_app_service.example.id
  resource_group_name = azurerm_resource_group.example.name
}

resource "azurerm_app_service_custom_hostname_binding" "example" {
  hostname = "subdomain2.example.com"
  web_app_id = azurerm_app_service.example.id
  resource_group_name = azurerm_resource_group.example.name
}

resource "azurerm_app_service_custom_hostname_binding" "example" {
  hostname = "subdomain3.example.com"
  web_app_id = azurerm_app_service.example.id
  resource_group_name = azurerm_resource_group.example.name
}

